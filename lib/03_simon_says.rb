def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string,count = 2)
   result = ""
   i = 0
   while  i < count
     if result == ""
       result = string
    else
      result += " " + string
    end
      i +=1
   end
   return result
end

def start_of_word(string,length)
  return string.slice(0..length - 1)
end

def first_word (word)
     arr_word = word.split(" ")
     arr_word[0]
end

def titleize(sentence)
   arr_sentence = sentence.split(" ")
   arr_sentence.each_with_index do |word , idx|
     if idx == 0 or idx == arr_sentence.length - 1
        arr_sentence[idx] = word.capitalize
     elsif arr_sentence[idx - 1].length < arr_sentence[idx].length
        arr_sentence[idx] = word.capitalize
     end
   end
  arr_sentence.join(" ")
end
