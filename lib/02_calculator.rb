def add(operand1,operand2)
   operand1 + operand2
end

def subtract(operand1,operand2)
   operand1 - operand2
end

def sum(arr)
  if arr.length == 0
     return 0
   else
     arr.reduce(:+)
  end
end

def multiply(arr)
   if arr.length == 0
     return 0
   else
     arr.reduce(:*)
   end
end

def power (first_number, second_number)
    first_number ** second_number
end

def factorial(n)
  return 1 if n == 0
  n * factorial(n - 1)
end
