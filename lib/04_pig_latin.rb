def translate(sentence)
  arr_sentence = sentence.split(" ")
 arr_sentence.each_with_index do |word, idx|
     arr_sentence[idx] = transform(word)
 end

arr_sentence.join(" ")
end

def transform(init)
  word =  ""
  suffix = ""
  if  init.split("").count { |el| "?!.,".include?(el)} > 0
    suffix = init[-1..-1]
    word = init[0..-2]
  else
    word = init + ""
  end
  if "aeiou".include?(word[0]) or "AEIOU".include?(word[0])
    word + "ay" + suffix
  else
    arr_word = word.split("")
    buffer = ""

        arr_word.each_with_index do |char,idx|
          if !"aeiou".include?(char) and !"AEIOU".include?(char)
             buffer += char
          else
             if buffer[-1] == "q" or buffer[-1] == "Q"
               buffer += char
               return word[idx + 1..-1] + buffer + "ay" + suffix
             end
            return  word[idx..-1]  + buffer + "ay" + suffix
          end
        end
  end
end
